### DEPRECATED ###
import os
import yaml
import json
import shutil
import datetime
import ROOT
import uproot
import numpy as np
import pandas as pd

# Define the path to the main directory
#main_directory = "./data"
#output_directory = "./potato"
# output_directory = "../potato/POTATO_data/LocalFiles/TestOutput"

class POTATO_Formatter():
    def __init__ ( self, pDirectory ):
        self.main_directory = pDirectory
        self.output_directory = pDirectory
        ROOT.gROOT.SetBatch(True)
    
    # Function to read a YAML file into a pandas DataFrame
    def load_data_from_yaml_to_dataframe(self, yaml_file_path, title, section):
        with open(yaml_file_path, 'r') as file:
            data = yaml.safe_load(file)

        # Extract the specific section data
        section_data = data.get(title, {}).get(section, [])

        # Assuming the structure contains lists of dictionaries with the keys we're interested in
        # Convert the list of dictionaries into a DataFrame
        df = pd.DataFrame(section_data)
        return df


    def load_info_from_yaml_to_json(self, yaml_file_path):
        # Load the YAML file
        with open(yaml_file_path, 'r') as file:
            data = yaml.safe_load(file)

        # Extract the required sections
        info = data.get('Info', {})
        summary = data.get('Summary', {})
        tasks = data.get('Tasks', [])

        # Combine into a single dictionary
        combined_data = {
                'Info': info,
                'Summary': summary,
                'Tasks': tasks
        }
        return combined_data


    # Function to append the data to the ROOT file
    def append_monitor_to_root_file(self, root_file_path, df_hv, df_lv, df_env):
        # Open the ROOT file and write the canvases
        root_file = ROOT.TFile.Open(root_file_path, "UPDATE")
        if not root_file or root_file.IsZombie():
            raise IOError(f"Could not open the ROOT file: {root_file_path}")

        # Create a directory for Monitor if it doesn't exist
        monitor_directory = root_file.GetDirectory("Monitor")
        if not monitor_directory:
            monitor_directory = root_file.mkdir("Monitor")
        monitor_directory.cd()

        df_hv_timestamps = [ROOT.TDatime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second) for dt in df_hv['Timestamps']]
        df_hv_timestamps = [td.Convert() for td in df_hv_timestamps]
        df_lv_timestamps = [ROOT.TDatime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second) for dt in df_lv['Timestamps']]
        df_lv_timestamps = [td.Convert() for td in df_lv_timestamps]
        df_env_timestamps = [ROOT.TDatime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second) for dt in df_env['Timestamps']]
        df_env_timestamps = [td.Convert() for td in df_env_timestamps]

        graph_hv_current = ROOT.TGraph(len(df_hv_timestamps),  np.array(df_hv_timestamps, dtype='float64'), df_hv['Currents'].to_numpy(dtype='float64'))
        graph_hv_current.SetTitle("HV monitor;Timestamp;Current [A]")
        graph_hv_current.SetMarkerStyle(20)  # Marker style for reference
        graph_hv_current.SetLineColor(ROOT.kBlue)
        graph_hv_current.SetLineWidth(2)
        graph_hv_current.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_hv_current.GetXaxis().SetTimeDisplay(1)
        graph_hv_current.GetXaxis().SetNdivisions(510)
        graph_hv_current.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_hv_current.GetXaxis().SetTimeOffset(0, "utc")

        graph_hv_current.Draw("AL")
        graph_hv_current.Write("HV_current")

        graph_hv_voltage = ROOT.TGraph(len(df_hv_timestamps),  np.array(df_hv_timestamps, dtype='float64'), df_hv['Voltages'].to_numpy(dtype='float64'))
        graph_hv_voltage.SetTitle("HV monitor;Timestamp;Voltage [V]")
        graph_hv_voltage.SetMarkerStyle(20)  # Marker style for reference
        graph_hv_voltage.SetLineColor(ROOT.kBlue)
        graph_hv_voltage.SetLineWidth(2)
        graph_hv_voltage.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_hv_voltage.GetXaxis().SetTimeDisplay(1)
        graph_hv_voltage.GetXaxis().SetNdivisions(510)
        graph_hv_voltage.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_hv_voltage.GetXaxis().SetTimeOffset(0, "utc")

        graph_hv_voltage.Draw("AL")
        graph_hv_voltage.Write("HV_voltage")

        graph_hv_ons = ROOT.TGraph(len(df_hv['Timestamps']), np.array(df_hv_timestamps, dtype='float64'), df_hv['Ons'].to_numpy(dtype='float64'))
        graph_hv_ons.SetTitle("HV monitor;Timestamp [UTC];Ons [bool]")
        graph_hv_ons.SetMarkerStyle(20)  # Marker style for reference
        graph_hv_ons.SetLineColor(ROOT.kBlue)
        graph_hv_ons.SetLineWidth(2)
        graph_hv_ons.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_hv_ons.GetXaxis().SetTimeDisplay(1)
        graph_hv_ons.GetXaxis().SetNdivisions(510)
        graph_hv_ons.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_hv_ons.GetXaxis().SetTimeOffset(0, "utc")

        graph_hv_ons.Draw("AL")
        graph_hv_ons.Write("HV_ons")

        graph_lv_current = ROOT.TGraph(len(df_lv['Timestamps']), np.array(df_lv_timestamps, dtype='float64'), df_lv['Currents'].to_numpy(dtype='float64'))
        graph_lv_current.SetTitle("LV monitor;Timestamp [UTC];Current [A]")
        graph_lv_current.SetMarkerStyle(20)  # Marker style for reference
        graph_lv_current.SetLineColor(ROOT.kBlue)
        graph_lv_current.SetLineWidth(2)
        graph_lv_current.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_lv_current.GetXaxis().SetTimeDisplay(1)
        graph_lv_current.GetXaxis().SetNdivisions(510)
        graph_lv_current.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_lv_current.GetXaxis().SetTimeOffset(0, "utc")

        graph_lv_current.Draw("AL")
        graph_lv_current.Write("LV_current")

        graph_lv_voltage = ROOT.TGraph(len(df_lv['Timestamps']), np.array(df_lv_timestamps, dtype='float64'), df_lv['Voltages'].to_numpy(dtype='float64'))
        graph_lv_voltage.SetTitle("LV monitor;Timestamp [UTC];Voltage [V]")
        graph_lv_voltage.SetMarkerStyle(20)  # Marker style for reference
        graph_lv_voltage.SetLineColor(ROOT.kBlue)
        graph_lv_voltage.SetLineWidth(2)
        graph_lv_voltage.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_lv_voltage.GetXaxis().SetTimeDisplay(1)
        graph_lv_voltage.GetXaxis().SetNdivisions(510)
        graph_lv_voltage.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_lv_voltage.GetXaxis().SetTimeOffset(0, "utc")

        graph_lv_voltage.Draw("AL")
        graph_lv_voltage.Write("LV_voltage")

        graph_lv_ons = ROOT.TGraph(len(df_lv['Timestamps']), np.array(df_lv_timestamps, dtype='float64'), df_lv['Ons'].to_numpy(dtype='float64'))
        graph_lv_ons.SetTitle("LV monitor;Timestamp [UTC];Ons [bool]")
        graph_lv_ons.SetMarkerStyle(20)  # Marker style for reference
        graph_lv_ons.SetLineColor(ROOT.kBlue)
        graph_lv_ons.SetLineWidth(2)
        graph_lv_ons.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_lv_ons.GetXaxis().SetTimeDisplay(1)
        graph_lv_ons.GetXaxis().SetNdivisions(510)
        graph_lv_ons.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_lv_ons.GetXaxis().SetTimeOffset(0, "utc")

        graph_lv_ons.Draw("AL")
        graph_lv_ons.Write("LV_ons")

        graph_env_temperature = ROOT.TGraph(len(df_env['Timestamps']), np.array(df_env_timestamps, dtype='float64'), df_env['Temperatures'].to_numpy(dtype='float64'))
        graph_env_temperature.SetTitle("Environment monitor;Timestamp;Temperature [degC]")
        graph_env_temperature.SetMarkerStyle(20)  # Marker style for reference
        graph_env_temperature.SetLineColor(ROOT.kBlue)
        graph_env_temperature.SetLineWidth(2)
        graph_env_temperature.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_env_temperature.GetXaxis().SetTimeDisplay(1)
        graph_env_temperature.GetXaxis().SetNdivisions(510)
        graph_env_temperature.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_env_temperature.GetXaxis().SetTimeOffset(0, "utc")

        graph_env_temperature.Draw("AL")
        graph_env_temperature.Write("ENV_temperature")

        graph_env_humidity = ROOT.TGraph(len(df_env['Timestamps']), np.array(df_env_timestamps, dtype='float64'), df_env['Humidities'].to_numpy(dtype='float64'))
        graph_env_humidity.SetTitle("Environment monitor;Timestamp [UTC];Humidity [%H]")
        graph_env_humidity.SetMarkerStyle(20)  # Marker style for reference
        graph_env_humidity.SetLineColor(ROOT.kBlue)
        graph_env_humidity.SetLineWidth(2)
        graph_env_humidity.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_env_humidity.GetXaxis().SetTimeDisplay(1)
        graph_env_humidity.GetXaxis().SetNdivisions(510)
        graph_env_humidity.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_env_humidity.GetXaxis().SetTimeOffset(0, "utc")

        graph_env_humidity.Draw("AL")
        graph_env_humidity.Write("ENV_humidity")

        graph_env_dewpoint = ROOT.TGraph(len(df_env['Timestamps']), np.array(df_env_timestamps, dtype='float64'), df_env['Dewpoints'].to_numpy(dtype='float64'))
        graph_env_dewpoint.SetTitle("Environment monitor;Timestamp [UTC];Dewpoint [degC]")
        graph_env_dewpoint.SetMarkerStyle(20)  # Marker style for reference
        graph_env_dewpoint.SetLineColor(ROOT.kBlue)
        graph_env_dewpoint.SetLineWidth(2)
        graph_env_dewpoint.SetLineStyle(ROOT.kSolid)
        # Configure x-axis to display time correctly
        graph_env_dewpoint.GetXaxis().SetTimeDisplay(1)
        graph_env_dewpoint.GetXaxis().SetNdivisions(510)
        graph_env_dewpoint.GetXaxis().SetTimeFormat("%H:%M:%S")
        graph_env_dewpoint.GetXaxis().SetTimeOffset(0, "utc")

        graph_env_dewpoint.Draw("AL")
        graph_env_dewpoint.Write("ENV_dewpoint")

        root_file.Close()


    def append_monitor_info_to_root_file(self, root_file_path, info_data):
        print(root_file_path)
        root_file = uproot.open(root_file_path)
        outfile = uproot.update(root_file_path)
        outfile["Info/Setup"] = "Gipht"

        if 'Summary' in info_data:
            for key, value in info_data['Summary'].items():
                if value is not None:  # Ensure the value is not None
                    formatted_key = "Summary/" + key
                    outfile[formatted_key] = value
        outfile["Info/Tasks"] = str(info_data['Tasks'])

        outfile.close()
        root_file.close()


    def append_summary_to_root_file(self, root_file_path, summary_data):
        print(root_file_path)
        root_file = uproot.open(root_file_path)
        outfile = uproot.update(root_file_path)

        for key, value in summary_data.items():
            if value is not None:  # Ensure the value is not None
                formatted_key = "Info/" + str(key)
                outfile[formatted_key] = str(value)
        outfile.close()
        root_file.close()


    def add_stat_box(self, canvas, graph, label, y_values):
        # Calculate statistical values
        min_y = min(y_values)
        max_y = max(y_values)
        mean_y = np.mean(y_values)

        # Create TPaveStats and set its properties
        stats = ROOT.TPaveStats(0.65, 0.65, 0.85, 0.85, "NDC")
        stats.SetBorderSize(1)
        stats.SetFillColor(ROOT.kWhite)
        stats.SetTextAlign(12)  # Left aligned text
        stats.SetTextSize(0.03)
        stats.SetTextColor(ROOT.kBlack)
        stats.SetLineColor(ROOT.kBlack)
        stats.SetLineWidth(1)

        # Add statistics text to the box
        stats.AddText(f"{label}")
        stats.AddText(f"Min {label}: {min_y:.2f}")
        stats.AddText(f"Max {label}: {max_y:.2f}")
        stats.AddText(f"Mean {label}: {mean_y:.2f}")

        # Position the stats box and add to the graph
        canvas.cd()
        graph.Draw("AL")
        canvas.Update()
        graph.SetStats(0)  # Disable automatic stats box
        graph.GetListOfFunctions().Add(stats)
        stats.Draw()


    def append_iv_to_root_file(self, root_file_path, df_iv):
        try:
            # Convert DataFrame columns to numpy arrays
            currents = df_iv['Current'].to_numpy(dtype='float64')
            voltages = df_iv['Voltage'].to_numpy(dtype='float64')
            humidity = df_iv['Relative Humidity'].to_numpy(dtype='float64')
            temperature = df_iv['Temperature'].to_numpy(dtype='float64')
            # timestamp = pd.to_datetime(df_iv['Timestamp']).astype(np.int64) // 10**9
            # timestamp = timestamp.to_numpy(dtype='float64')

            # Open the ROOT file
            root_file = ROOT.TFile.Open(root_file_path, "UPDATE")
            if not root_file or root_file.IsZombie():
                raise IOError(f"Could not open the ROOT file: {root_file_path}")

            # Create a directory for IV if it doesn't exist
            iv_directory = root_file.GetDirectory("IV")
            if not iv_directory:
                iv_directory = root_file.mkdir("IV")
            iv_directory.cd()

            n_points = len(voltages)

            # Create canvases
            # c_iv = ROOT.TCanvas("c_iv", "IV Curve", 800, 600)
            # c_temp = ROOT.TCanvas("c_temp", "Temperature vs Voltage", 800, 600)
            # c_humidity = ROOT.TCanvas("c_humidity", "Humidity vs Voltage", 800, 600)
            # c_timestamp = ROOT.TCanvas("c_timestamp", "Timestamp vs Voltage", 800, 600)

            # Create graphs
            graph_iv = ROOT.TGraph(n_points, voltages, currents)
            graph_iv.SetTitle("IV Curve;Voltage [V];Current [nA]")
            graph_iv.SetMarkerStyle(20)  # Marker style for reference
            graph_iv.SetLineColor(ROOT.kBlue)
            graph_iv.SetLineWidth(2)
            graph_iv.SetLineStyle(ROOT.kSolid)
            # c_iv.cd()
            # graph_iv.Draw("AL")
            # c_iv.Update()
            graph_iv.Draw("AL")
            graph_iv.Write("IV_current")

            graph_temp = ROOT.TGraph(n_points, voltages, temperature)
            graph_temp.SetTitle("Temperature vs Voltage;Voltage [V];Temperature [degC]")
            graph_temp.SetMarkerStyle(20)  # Marker style for reference
            graph_temp.SetLineColor(ROOT.kRed)
            graph_temp.SetLineWidth(2)
            graph_temp.SetLineStyle(ROOT.kSolid)
            # c_temp.cd()
            # graph_temp.Draw("AL")
            # c_temp.Update()
            graph_temp.Draw("AL")
            graph_temp.Write("IV_temperature")

            graph_humidity = ROOT.TGraph(n_points, voltages, humidity)
            graph_humidity.SetTitle("Humidity vs Voltage;Voltage [V];Humidity [H]")
            graph_humidity.SetMarkerStyle(20)  # Marker style for reference
            graph_humidity.SetLineColor(ROOT.kGreen)
            graph_humidity.SetLineWidth(2)
            graph_humidity.SetLineStyle(ROOT.kSolid)
            # c_humidity.cd()
            # graph_humidity.Draw("AL")
            # c_humidity.Update()
            graph_humidity.Draw("AL")
            graph_humidity.Write("IV_humidity")

            df_iv_timestamps = [ROOT.TDatime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second) for dt in df_iv['Timestamp']]
            df_iv_timestamps = [td.Convert() for td in df_iv_timestamps]

            graph_timestamp = ROOT.TGraph(len(df_iv_timestamps), voltages, np.array(df_iv_timestamps, dtype='float64'))
            # graph_timestamp = ROOT.TGraph(n_points, voltages, timestamp)
            graph_timestamp.SetTitle("Timestamp vs Voltage;Voltage [V];Timestamp [UTC]")
            graph_timestamp.SetMarkerStyle(20)  # Marker style for reference
            graph_timestamp.SetLineColor(ROOT.kBlack)
            graph_timestamp.SetLineWidth(2)
            graph_timestamp.SetLineStyle(ROOT.kSolid)
            # Configure x-axis to display time correctly
            graph_timestamp.GetYaxis().SetTimeDisplay(1)
            graph_timestamp.GetYaxis().SetNdivisions(510)
            graph_timestamp.GetYaxis().SetTimeFormat("%H:%M:%S")
            graph_timestamp.GetYaxis().SetTimeOffset(0, "utc")

            graph_timestamp.Draw("AL")
            graph_timestamp.Write("IV_timestamp")


            # Add statistical boxes (TPaveStats) to each canvas
            # add_stat_box(c_iv, graph_iv, "Current", currents)
            # add_stat_box(c_temp, graph_temp, "Temperature", temperature)
            # add_stat_box(c_humidity, graph_humidity, "Humidity", humidity)
            # add_stat_box(c_humidity, graph_humidity, "Timestamp", humidity)

            # Close the ROOT file
            root_file.Close()

        except Exception as e:
            print("IV ERROR:", e)


    def do_format(self):
        i=0
        # Walk through the directory structure       
        for root, dirs, files in os.walk(self.main_directory):
            if 'Results.root' in files:
                print(root)
                root_file = os.path.join(root, 'Results.root')
                monitor_files = [os.path.join(root, f) for f in files if f.startswith("Monitor") and f.endswith(".yml")]
                monitor_summary_files = [os.path.join(root, f) for f in files if f.startswith("summary_AnalyseMonitorData") and f.endswith(".yml")]
                monitor_file = monitor_files[0] if monitor_files else None
                monitor_file_exits = os.path.exists(monitor_file) if monitor_file is not None else False
                monitor_summary_file = monitor_summary_files[0] if monitor_files else None
                monitor_summary_file_exist = os.path.exists(monitor_summary_file) if monitor_summary_file is not None else False
                iv_files = [os.path.join(root, f) for f in files if f.startswith("IV_") and f.endswith(".yml")]
                iv_file = iv_files[0] if iv_files else None
                iv_file_exist = os.path.exists(iv_file) if iv_file is not None else False
                i += 1

                if monitor_file_exits:
                    try:
                        df_hv = self.load_data_from_yaml_to_dataframe(monitor_file, title='Data', section='HV')
                        df_lv = self.load_data_from_yaml_to_dataframe(monitor_file, title='Data', section='LV')
                        df_env = self.load_data_from_yaml_to_dataframe(monitor_file, title='Data', section='Environment')
                        info_data = self.load_info_from_yaml_to_json(monitor_file)

                        df_hv['Timestamps'] = pd.to_datetime(df_hv['Timestamps'])
                        df_lv['Timestamps'] = pd.to_datetime(df_lv['Timestamps'])
                        df_env['Timestamps'] = pd.to_datetime(df_env['Timestamps'])
                        df_env['Temperatures'] = pd.to_numeric(df_env['Temperatures'], errors='coerce')
                        df_env['Dewpoints'] = pd.to_numeric(df_env['Dewpoints'], errors='coerce')
                        df_env['Humidities'] = pd.to_numeric(df_env['Humidities'], errors='coerce')
                        columns_to_convert = ['Currents', 'Voltages', 'Ons']
                        for column in columns_to_convert:
                            df_hv[column] = pd.to_numeric(df_hv[column], errors='coerce')
                            df_lv[column] = pd.to_numeric(df_lv[column], errors='coerce')

                        print(f"Successfully read {monitor_file}")

                    except Exception as e:
                        print(f"Failed to read {monitor_file}: {e}")

                if monitor_summary_file_exist:
                    try:
                        with open(monitor_summary_file, 'r') as file:
                            data = yaml.safe_load(file)
                        summary_data = data.get('Info', {})
                        print(summary_data)

                        print(f"Successfully read {monitor_summary_file}")
                    except Exception as e:
                        print(f"Failed to read {monitor_summary_file}: {e}")

                if iv_file is not None:
                    try:
                        with open(iv_file, 'r') as file:
                            data_iv = yaml.safe_load(file)

                        # Convert to DataFrame
                        df_iv = pd.DataFrame(data_iv['Data'])
                        info_data = self.load_info_from_yaml_to_json(monitor_file)

                        df_iv['Timestamp'] = pd.to_datetime(df_iv['Timestamp'])
                        columns_to_convert = ['Current', 'Voltage', 'Relative Humidity', 'Temperature']
                        for column in columns_to_convert:
                            df_iv[column] = pd.to_numeric(df_iv[column], errors='coerce')

                        print(f"Successfully read {iv_file}")

                    except Exception as e:
                        print(f"Failed to read {iv_file}: {e}")

                if os.path.exists(root_file):
                    try:
                        # Get the POTATO naming information
                        module_name = info_data['Info']['ID']
                        test_timestamp = datetime.datetime.strptime(summary_data['Date'], '%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d_%Hh%Mm%Ss')
                        temp = int(float(info_data['Summary']['T at start of ModuleTest (T)']))
                        temp = f"+{temp}C" if temp > 0 else f"{temp}C"
                        ph2acf = "v1-00"

                        # Copy the ROOT file to the POTATO directory and rename accordingly
                        potato_file_name_convention = f"{module_name}_{test_timestamp}_{temp}_{ph2acf}"
                        potato_file = f"{self.output_directory}/{potato_file_name_convention}.root"
                        shutil.copy(root_file, potato_file)
                    except Exception as e:
                        print("error coping root file", e)
                        continue

                    if monitor_file_exits:
                        # Append the DataFrames  & Information to the ROOT file
                        print("saving the data into the original ROOT file")
                        self.append_monitor_to_root_file(potato_file, df_hv=df_hv, df_lv=df_lv, df_env=df_env)
                        self.append_monitor_info_to_root_file(potato_file, info_data=info_data)

                    if monitor_summary_file_exist:
                        # Append the Summary data to the ROOT file
                        print("saving the summary data into the original ROOT file")
                        self.append_summary_to_root_file(potato_file, summary_data=summary_data)

                    if iv_file_exist:
                        # Append the DataFrames to the ROOT file
                        print("saving the iv data into the original ROOT file")
                        self.append_iv_to_root_file(potato_file, df_iv=df_iv)
        return potato_file

