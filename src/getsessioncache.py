from optparse import OptionParser
import requests
import sys
import os
import re
import logging
import tempfile
import subprocess
import json
import warnings
from base64 import b64encode, b64decode
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
from PySide2.QtWidgets import QInputDialog
from Dialog.LoginDBDialog import LoginDBDialog
from PySide2.QtCore import Signal, QObject
import urllib3
from urllib.parse import urlparse

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class GiphtSSO(QObject):
    login = Signal ( )

    DEFAULT_TIMEOUT_SECONDS = 10


    def __init__( self, p2FA):
        super(GiphtSSO, self).__init__(  )
        self.twoFA = "2fa" if p2FA else "simple"

    def login_page1(self, html_content,username,password):
        soup = BeautifulSoup(html_content, 'html.parser')
        login_form = soup.find('form', {'id': 'kc-form-login'})
        action_url = login_form['action']
        form_data = {}
        for input_tag in soup.find_all('input'):
            name = input_tag.get('name')
            value = input_tag.get('value', '')
            form_data[name] = value

        form_data['username'] = username
        form_data['password'] = password

        return action_url,form_data

    def findformeotppage(self, html_content):   
        soup = BeautifulSoup(html_content, 'html.parser')
        otp_form = soup.find('form', {'id': 'kc-otp-login-form'})
        action_url = otp_form['action']
        method = otp_form['method']

        otp, ok = QInputDialog.getText(None, 'OTP', 'Please Enter OTP for DB Connection')
        #otp = input("Otp: ")
        if ok:
            otp_input = otp_form.find('input', {'id': 'otp'})
            otp_input['value'] = otp
            form_data = {}
            for input_tag in otp_form.find_all('input'):
                form_data[input_tag.get('name')] = input_tag.get('value', '')
            return action_url,form_data

    def html_root(self, r):
        c = r.content.decode('utf-8')
        c = re.sub('<meta [^>]*>','', c, flags = re.IGNORECASE)
        c = re.sub('<hr>','', c, flags = re.IGNORECASE)
        c = re.sub("=\\'([^']*)\\'",'="\g<1>"', c, flags = re.IGNORECASE)
        c = re.sub(" autofocus "," ", c, flags = re.IGNORECASE)
        c = re.sub('<img ([^>]*)>','<img \g<1>/>',c, flags = re.IGNORECASE)
        c = re.sub('<script>[^<]*</script>','',c, flags = re.IGNORECASE)
        #print("converting page")
        return ET.fromstring(c)
        

    def file_mtime(self, file_path):
        try:
            return os.path.getmtime(file_path)
        except OSError:
            return -1

    def read_form(self, r):
        root = self.html_root(r)
        #form = root.find(".//{http://www.w3.org/1999/xhtml}form")
        form = root.find(".//form")
        action = form.get('action')
        #form_data = dict(((e.get('name'), e.get('value')) for e in form.findall(".//{http://www.w3.org/1999/xhtml}input")))
        form_data = dict(((e.get('name'), e.get('value')) for e in form.findall(".//input")))
        return action, form_data

    def is_email(self, s):
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
        return re.search(regex, s)

    def split_url(self, url):
        a = len(urlparse(url).query) + len(urlparse(url).path)
        return url[:-(a + 1)], url[len(url) - a - 1:]

    def enteredCredentials( self, pCredentials ):
        ret = self.login_sign_on(url="https://cmsdca.cern.ch/trk_rhapi/" ,login_type=self.twoFA, username=pCredentials[0], password=pCredentials[1])      
        self.cacheFile = ret
        self.login.emit()

    def showDialog( self ):
        logging.warning('Credentials will be stored in a NOT secure way!')
        self.log = LoginDBDialog()
        self.log.sendCredentials.connect(self.enteredCredentials)
        self.log.show()

    def log_in(self):
        ret = self.login_sign_on(url="https://cmsdca.cern.ch/trk_rhapi/" ,login_type=self.twoFA)#, username=pCredentials[0], password=pCredentials[1])
        if ret is not None:
            self.login.emit()

    def login_sign_on(self, url, login_type, cache_file = ".session.cache", force_level = 1, username='', password=''):
        from ilock import ILock
        from getpass import getpass
        from os import remove, path

        cache = None
        cache_file = path.abspath(cache_file)
        cache_lock_id = b64encode(cache_file.encode('utf-8')).decode()
        cache_time = self.file_mtime(cache_file)

        #print("Logging: ", username)
        with ILock(cache_lock_id):

            if force_level == 1 and cache_time != self.file_mtime(cache_file):
                force_level = 0

            if force_level == 2:
                #print("Removing session file")
                try: 
                    os.remove(cache_file) 
                    #print("% s removed successfully" % cache_file) 
                except OSError as error:
                    pass
                    #print(error) 
                    #print("% s does not exist." % cache_file) 

            if path.isfile(cache_file):
                logging.debug('%s found', cache_file)
                with open(cache_file, 'r') as f:
                    cache = json.loads(f.read())

            else:
                logging.debug('%s not found', cache_file)

            if force_level > 0 or cache is None or 'cookies' not in cache:
               
                if cache is not None and 'secret' in cache:
                    secret = b64decode(cache['secret'].encode()).decode()
                    username, password = secret.split('/')
                    password = b64decode(password.encode()).decode()
                else:
                    if password =='' and username == '':
                        #username = input("Username: ")
                        #password = getpass("Password: ")
                        self.showDialog()
                        return

                with requests.Session() as s:

                    r1 = s.get(url, timeout = 10, verify = False, allow_redirects = True)
                    r1.raise_for_status()

                    if self.is_email(username):
                        logging.debug("%s is guest account." % username)
                        root = self.html_root(r1)
                        link = root.find(".//{http://www.w3.org/1999/xhtml}a[@id='zocial-guest']")
                        guest_url = self.split_url(r1.url)[0] + self.split_url(link.get('href'))[1]
                        logging.debug(guest_url)
                        r1 = s.get(guest_url, timeout = 10, verify = False, allow_redirects = True)
                        r1.raise_for_status()

                    else:
                        #print("Regular account")
                        logging.debug("%s is a regular account." % username)

                    action,form_data=self.login_page1(r1.content.decode('utf-8'),username,password)
                    # # action, form_data = self.read_form(r1)


                   # # form_data['username'] = username
                    # # form_data['password'] = password

                    r2 = s.post(url=action, data=form_data, timeout=self.DEFAULT_TIMEOUT_SECONDS, allow_redirects=True)
                    r2.raise_for_status()
                    if login_type=='simple':
                        try:
                            action, form_data = self.read_form(r2)
                        except ET.ParseError as error:
                            #print(error) 
                            print("Error parsing html file. Make sure that the password is correct and that the account \"" + username + "\" exists and has been registered in the database." )
                            return error

                        #print("Done Getting account info")

                        r3 = s.post(url=action, data=form_data, timeout=self.DEFAULT_TIMEOUT_SECONDS, allow_redirects=True)
                    elif login_type=='2fa':
                        try:
                            action,form_data=self.findformeotppage(r2.content.decode('utf-8'))
                            r3 = s.post(url=action, data=form_data, timeout=self.DEFAULT_TIMEOUT_SECONDS, allow_redirects=True)
                            action, form_data = self.read_form(r3)
                            r4 = s.post(url=action, data=form_data, timeout=self.DEFAULT_TIMEOUT_SECONDS, allow_redirects=True)
                        except:
                            print("GIPHT:\tLoign Error ")
                            return None

                    cache = {
                        'secret': b64encode((username + '/' + b64encode(password.encode()).decode()).encode()).decode(),
                        'location': url,
                        'cookies': {c.name: c.value for c in s.cookies}
                    }

                    with open(cache_file, 'w') as f:
                        f.write(json.dumps(cache))
                        f.close()

                    # print(cache['cookies'])
                    # #cookie_file = ".cookie.cache"
                    # with open('.cookie.cache', 'w') as t:
                    #     t.write(json.dumps(cache['cookies']))
                    #     t.close()

            return cache['cookies']
