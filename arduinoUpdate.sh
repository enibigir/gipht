cd arduino-cli/
./install.sh
./bin/arduino-cli core install arduino:avr
./bin/arduino-cli config set directories.user ../KIRArduino/
./bin/arduino-cli compile -b arduino:avr:micro ../KIRArduino/
./bin/arduino-cli upload -p $1 --fqbn arduino:avr:micro ../KIRArduino/
cd ..